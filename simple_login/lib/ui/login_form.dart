import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginState();
  }
}

class LoginState extends State<Login> {
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  String _username = 'Forgot Password?';

  @override
  Widget build(BuildContext context) {
    final logo = new Hero(
        tag: 'logo',
        child: SizedBox(
          width: 150.0,
          height: 150.0,
          child: FlatButton(
            onPressed: () {
              setState(() {});
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => new MaterialApp(
                            home: Scaffold(
                              appBar: AppBar(
                                title: Text('Second Page'),
                              ),
                              body: ListView(
                                padding: EdgeInsets.all(10.0),
                                shrinkWrap: true,
                                children: <Widget>[
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  Hero(
                                    tag: 'logo',
                                    child: Image.asset(
                                      'images/man.png',
                                      width: 200.0,
                                      height: 200.0,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text(
                                      "Lorem Ipsum is simply dummy text of the printing"
                                      " and typesetting industry. Lorem Ipsum has been "
                                      "the industry's standard dummy text ever since the 1500s, "
                                      "when an unknown printer took a galley of type and"
                                      " scrambled it to make a type specimen book. It has survived "
                                      "not only five centuries, but also the leap into electronic"
                                      " typesetting, remaining essentially unchanged. It was "
                                      "popularised in the 1960s with the release of Letraset "
                                      "sheets containing Lorem Ipsum passages, and more recently "
                                      "with desktop publishing software like Aldus PageMaker "
                                      "including versions of Lorem Ipsum.",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  textAlign: TextAlign.justify,)
                                ],
                              ),
                            ),
                          )));
            },
            shape: CircleBorder(),
            child: Image.asset(
              'images/man.png',
            ),
          ),
        ));

    final username = new TextField(
      maxLines: 1,
      maxLength: 15,
      cursorColor: Colors.blueAccent,
      cursorRadius: Radius.circular(5.0),
      cursorWidth: 5.0,
      autocorrect: true,
      keyboardType: TextInputType.text,
      autofocus: false,
      controller: _userController,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.person),
          labelText: 'Username',
          hintStyle: TextStyle(
              color: Colors.grey.shade700, fontWeight: FontWeight.bold),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final password = new TextField(
      maxLines: 1,
      maxLength: 10,
      cursorColor: Colors.blueAccent,
      cursorRadius: Radius.circular(5.0),
      cursorWidth: 5.0,
      autocorrect: false,
      autofocus: false,
      controller: _passwordController,
      decoration: InputDecoration(
        labelText: 'Password',
        prefixIcon: Icon(Icons.lock),
        hintStyle:
            TextStyle(color: Colors.grey.shade700, fontWeight: FontWeight.bold),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      obscureText: true,
    );

    final loginButton = RaisedButton(
      padding: EdgeInsets.all(20.0),
      shape: StadiumBorder(),
      color: Colors.blueAccent,
      elevation: 8.0,
      splashColor: Colors.blueAccent.shade100,
      child: Text(
        'Login',
        style: TextStyle(color: Colors.white),
      ),
      onPressed: _showWelcome,
    );

    final forgotLabel = FlatButton(
      child: Text(
        '$_username',
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () {},
    );

    return new Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
          centerTitle: true,
          title: new Text('Login'),
          backgroundColor: Colors.blueAccent,
        ),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(
                height: 20.0,
              ),
              username,
              SizedBox(
                height: 8.0,
              ),
              password,
              SizedBox(height: 48.0),
              loginButton,
              SizedBox(
                height: 8.0,
              ),
              forgotLabel
            ],
          ),
        ));
  }

  void _showWelcome() {
    setState(() {
      if (_userController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty) {
        _username = _userController.text.toString();
      }
    });
  }
}
